/* eslint-disable no-undef */
const app = require('../src/server');
const request = require('supertest');
const chai = require('chai')

const expect = chai.expect;

describe("#GET /employees ", () => {
    const url = '/employees'
    it("should get valid json response", (done) => {
        request(app)
            .get(url)
            .expect('Content-Type', /json/)
            .expect(200, done);
    })

    it("should have standard response structure", (done) => {
        request(app)
            .get(url)
            .end((err, res) => {
                expect(res.body).to.have.property('success');
                expect(res.body).to.have.property('message');
                expect(res.body).to.have.property('data');
                expect(res.body).to.have.property('errors');
                done();
            })
    })

    it("should get valid array", async () => {
        const res = await request(app).get(url);
        expect(res.body.data).to.be.an('array');
    })
})

describe("#GET /employees/:id ", () => {
    const url = '/employees/EN_0001';
    it("should get valid json response", (done) => {
        request(app)
            .get(url)
            .expect('Content-Type', /json/)
            .expect(200, done);
    })

    it("should have standard response structure", (done) => {
        request(app)
            .get(url)
            .end((err, res) => {
                expect(res.body).to.have.property('success');
                expect(res.body).to.have.property('message');
                expect(res.body).to.have.property('data');
                expect(res.body).to.have.property('errors');
                done();
            })
    })

    it("should get valid object", async () => {
        const res = await request(app).get(url);
        expect(res.body.data).to.be.an('object');
    })

    it("should have required properties on employee", (done) => {
        request(app)
            .get(url)
            .end( (err, res) => {
                expect(res.body.data).to.have.property('employeeId');
                expect(res.body.data).to.have.property('fullName');
                expect(res.body.data).to.have.property('branch');
                expect(res.body.data).to.have.property('salary');
                expect(res.body.data).to.have.property('payeTax');
                expect(res.body.data).to.have.property('netPay');
                done();
            })
    })
})
