const express = require('express')
const router = express.Router();
const employeeView = require('./view');

router.get('/', async (req, res) => {
    let employeeData = await employeeView.getAllEmployees();

    if(employeeData) {
        res.status(200).json({
            success: true,
            message: "Employees found",
            data: employeeData,
            errors: []
        })
    } else {
        res.status(404).json({
            success: false,
            message: "",
            data: {},
            errors: [
                "Employees not found"
            ]
        })
    }
})

router.get('/:id', async (req, res) => {
    let employee = await employeeView.getEmployeeDetails(req.params.id);
    if(employee) {
        res.status(200).json({
            success: true,
            message: "Employee found",
            data: employee,
            errors: []
        })
    } else {
        res.status(404).json({
            success: false,
            message: "",
            data: {},
            errors: [
                "Employee not found"
            ]
        })
    }
})

module.exports = router
