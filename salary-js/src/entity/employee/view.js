const employeeModel = require('./model');
const employeeService = require('./service');

const getAllEmployees = async () => {
    return await employeeModel.getAllEmployees();
}

const getEmployeeDetails = (id) => {
    return employeeService.getEmployeeDetails(id);
}

module.exports = {
    getAllEmployees,
    getEmployeeDetails
}
